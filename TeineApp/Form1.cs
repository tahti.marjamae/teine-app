﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace TeineApp
{
    public partial class Form1 : Form
    {
        List<Inimene> Inimesed = new List<Inimene>
        {
            new Inimene {EesNimi="Henn", PereNimi="Sarv", Vanus = 64},
            new Inimene {EesNimi="Ants", PereNimi="Saunamees", Vanus = 40},
            new Inimene {EesNimi="Peeter", PereNimi="Suur", Vanus = 28},
        };
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTere_Click(object sender, EventArgs e)
        {
            this.labelPeidus.Text = $"Tere { this.txtNimi.Text}!";
        }

        private void buttonLoeRahvas_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = Inimesed;
        }

        private void buttonKustutame_Click(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = null;
        }

        private void buttonLisa_Click(object sender, EventArgs e)
        {
            Inimesed.Add(new Inimene
            {
                EesNimi = this.textBoxEesnimi.Text,
                PereNimi = this.textBoxPerenimi.Text,
                Vanus = int.TryParse(this.textBoxVanus.Text, out int vanus) ? vanus : 0

            }) ;
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Inimesed;
        }

        private void buttonLoeKettalt_Click(object sender, EventArgs e)
        {
            Inimesed = JsonConvert.DeserializeObject<List<Inimene>>(
                System.IO.File.ReadAllText("..\\..\\inimesed.json")

                );
            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Inimesed;
        }

        private void buttonSalvesta_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText("..\\..\\inimesed.json",
                JsonConvert.SerializeObject(Inimesed)
                );
        }
    }
}
