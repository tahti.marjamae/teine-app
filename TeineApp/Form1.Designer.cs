﻿namespace TeineApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNimi = new System.Windows.Forms.TextBox();
            this.btnTere = new System.Windows.Forms.Button();
            this.buttonLoeRahvas = new System.Windows.Forms.Button();
            this.buttonKustutame = new System.Windows.Forms.Button();
            this.buttonSalvesta = new System.Windows.Forms.Button();
            this.buttonLoeKettalt = new System.Windows.Forms.Button();
            this.buttonLisa = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEesnimi = new System.Windows.Forms.TextBox();
            this.textBoxPerenimi = new System.Windows.Forms.TextBox();
            this.textBoxVanus = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.labelPeidus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNimi
            // 
            this.txtNimi.Location = new System.Drawing.Point(72, 34);
            this.txtNimi.Name = "txtNimi";
            this.txtNimi.Size = new System.Drawing.Size(100, 20);
            this.txtNimi.TabIndex = 0;
            // 
            // btnTere
            // 
            this.btnTere.Location = new System.Drawing.Point(225, 34);
            this.btnTere.Name = "btnTere";
            this.btnTere.Size = new System.Drawing.Size(75, 23);
            this.btnTere.TabIndex = 1;
            this.btnTere.Text = "Tere";
            this.btnTere.UseVisualStyleBackColor = true;
            this.btnTere.Click += new System.EventHandler(this.btnTere_Click);
            // 
            // buttonLoeRahvas
            // 
            this.buttonLoeRahvas.Location = new System.Drawing.Point(225, 76);
            this.buttonLoeRahvas.Name = "buttonLoeRahvas";
            this.buttonLoeRahvas.Size = new System.Drawing.Size(75, 23);
            this.buttonLoeRahvas.TabIndex = 2;
            this.buttonLoeRahvas.Text = "LoeRahvas";
            this.buttonLoeRahvas.UseVisualStyleBackColor = true;
            this.buttonLoeRahvas.Click += new System.EventHandler(this.buttonLoeRahvas_Click);
            // 
            // buttonKustutame
            // 
            this.buttonKustutame.Location = new System.Drawing.Point(370, 76);
            this.buttonKustutame.Name = "buttonKustutame";
            this.buttonKustutame.Size = new System.Drawing.Size(75, 23);
            this.buttonKustutame.TabIndex = 3;
            this.buttonKustutame.Text = "Kustutame";
            this.buttonKustutame.UseVisualStyleBackColor = true;
            this.buttonKustutame.Click += new System.EventHandler(this.buttonKustutame_Click);
            // 
            // buttonSalvesta
            // 
            this.buttonSalvesta.Location = new System.Drawing.Point(72, 351);
            this.buttonSalvesta.Name = "buttonSalvesta";
            this.buttonSalvesta.Size = new System.Drawing.Size(75, 23);
            this.buttonSalvesta.TabIndex = 4;
            this.buttonSalvesta.Text = "Salvesta";
            this.buttonSalvesta.UseVisualStyleBackColor = true;
            this.buttonSalvesta.Click += new System.EventHandler(this.buttonSalvesta_Click);
            // 
            // buttonLoeKettalt
            // 
            this.buttonLoeKettalt.Location = new System.Drawing.Point(72, 407);
            this.buttonLoeKettalt.Name = "buttonLoeKettalt";
            this.buttonLoeKettalt.Size = new System.Drawing.Size(75, 23);
            this.buttonLoeKettalt.TabIndex = 5;
            this.buttonLoeKettalt.Text = "LoeKettalt";
            this.buttonLoeKettalt.UseVisualStyleBackColor = true;
            this.buttonLoeKettalt.Click += new System.EventHandler(this.buttonLoeKettalt_Click);
            // 
            // buttonLisa
            // 
            this.buttonLisa.Location = new System.Drawing.Point(647, 234);
            this.buttonLisa.Name = "buttonLisa";
            this.buttonLisa.Size = new System.Drawing.Size(75, 23);
            this.buttonLisa.TabIndex = 6;
            this.buttonLisa.Text = "Lisa";
            this.buttonLisa.UseVisualStyleBackColor = true;
            this.buttonLisa.Click += new System.EventHandler(this.buttonLisa_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(581, 132);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Eesnimi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(581, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Perenimi";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(581, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Vanus";
            // 
            // textBoxEesnimi
            // 
            this.textBoxEesnimi.Location = new System.Drawing.Point(647, 125);
            this.textBoxEesnimi.Name = "textBoxEesnimi";
            this.textBoxEesnimi.Size = new System.Drawing.Size(100, 20);
            this.textBoxEesnimi.TabIndex = 10;
            // 
            // textBoxPerenimi
            // 
            this.textBoxPerenimi.Location = new System.Drawing.Point(647, 163);
            this.textBoxPerenimi.Name = "textBoxPerenimi";
            this.textBoxPerenimi.Size = new System.Drawing.Size(100, 20);
            this.textBoxPerenimi.TabIndex = 11;
            // 
            // textBoxVanus
            // 
            this.textBoxVanus.Location = new System.Drawing.Point(647, 201);
            this.textBoxVanus.Name = "textBoxVanus";
            this.textBoxVanus.Size = new System.Drawing.Size(100, 20);
            this.textBoxVanus.TabIndex = 12;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(72, 132);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(482, 145);
            this.dataGridView1.TabIndex = 13;
            // 
            // labelPeidus
            // 
            this.labelPeidus.AutoSize = true;
            this.labelPeidus.Location = new System.Drawing.Point(342, 34);
            this.labelPeidus.Name = "labelPeidus";
            this.labelPeidus.Size = new System.Drawing.Size(0, 13);
            this.labelPeidus.TabIndex = 14;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelPeidus);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.textBoxVanus);
            this.Controls.Add(this.textBoxPerenimi);
            this.Controls.Add(this.textBoxEesnimi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonLisa);
            this.Controls.Add(this.buttonLoeKettalt);
            this.Controls.Add(this.buttonSalvesta);
            this.Controls.Add(this.buttonKustutame);
            this.Controls.Add(this.buttonLoeRahvas);
            this.Controls.Add(this.btnTere);
            this.Controls.Add(this.txtNimi);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNimi;
        private System.Windows.Forms.Button btnTere;
        private System.Windows.Forms.Button buttonLoeRahvas;
        private System.Windows.Forms.Button buttonKustutame;
        private System.Windows.Forms.Button buttonSalvesta;
        private System.Windows.Forms.Button buttonLoeKettalt;
        private System.Windows.Forms.Button buttonLisa;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEesnimi;
        private System.Windows.Forms.TextBox textBoxPerenimi;
        private System.Windows.Forms.TextBox textBoxVanus;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label labelPeidus;
    }
}

